/* Homework 12
 * create  Java command line application to take a zip and display weather information
 * Programmer: Ian Harbison Spring 2019
 */
 
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.net.URL;
import com.google.gson.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Wxhw {

    /*get weather data from openweathermap.org using the zip code*/
    public String getWeather(String inputZip)
    {
		JsonElement jse = null;
        String report = null;

		try
		{
			// Construct openweathermap.org API URL
			URL inputURL = new URL("http://api.openweathermap.org/data/2.5/weather?zip=" 
                + inputZip + ",us&units=imperial&APPID=2ec036bb3d358d05cf040e98b4473c09");

			// Open the URL
			InputStream is = inputURL.openStream(); // throws an IOException
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			// Read the result into a JSON Element
			jse = new JsonParser().parse(br);
      
			// Close the connection
			is.close();
			br.close();
		}
		catch (java.io.UnsupportedEncodingException uee)
		{
			uee.printStackTrace();
		}
		catch (java.net.MalformedURLException mue)
		{
			mue.printStackTrace();
		}
		catch (java.io.IOException ioe)
		{
			System.out.println("ERROR: no cities match your search query");
		}

		if (jse != null)
		{
        // Create a weather report

        String place = jse.getAsJsonObject().get("name").getAsString();
        report = "Location:\t\t" + place + "\n";
        
        String time = jse.getAsJsonObject().get("dt").getAsString();
        long unix_time = Long.parseLong(time);
        Date date = new Date(unix_time*1000L);
        SimpleDateFormat jdf = new SimpleDateFormat("MMMM dd HH:mm z");
        String java_date = jdf.format(date);
        
        report = report + "Time:\t\t\tLast Updated on " + java_date + "\n";
        
        JsonArray obs = jse.getAsJsonObject().get("weather").getAsJsonArray();
        String conditions = obs.get(0).getAsJsonObject().get("main").getAsString();
        report = report + "Weather:\t\t" + conditions + "\n";
        
        String temp = jse.getAsJsonObject().get("main").getAsJsonObject().get("temp").getAsString();
        report = report + "Temperature F:\t" + temp + "\n";
        
        String windSpd = jse.getAsJsonObject().get("wind").getAsJsonObject().get("speed").getAsString();
        String windDir = jse.getAsJsonObject().get("wind").getAsJsonObject().get("deg").getAsString();
        report = report + "Wind:\t\t\tFrom " + windDir + " degrees at " + windSpd + " MPH" + "\n";
        
        String pressure = jse.getAsJsonObject().get("main").getAsJsonObject().get("pressure").getAsString();
        double pinHG = Double.parseDouble(pressure);
        pinHG = pinHG * 0.02953;
        report = report + "Pressure inHG:\t" + String.format( "%.2f", pinHG);
        }
        return report;
    }
    
    public static void main(String[] args)
	{
		Wxhw b = new Wxhw();
		String wxReport = b.getWeather(args[0]);
        if (wxReport != null) {
		    System.out.println(wxReport);
        }
    }
}